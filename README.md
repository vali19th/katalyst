# katalytic-files [![version](https://img.shields.io/pypi/v/katalytic-files)](https://pypi.org/project/katalytic-files/) [![tests](https://gitlab.com/katalytic/katalytic-files/badges/main/pipeline.svg?key_text=tests&key_width=38)](https://gitlab.com/katalytic/katalytic-files/-/commits/main) [![coverage](https://gitlab.com/katalytic/katalytic-files/badges/main/coverage.svg)](https://gitlab.com/katalytic/katalytic-files/-/commits/main) [![docs](https://img.shields.io/readthedocs/katalytic-files.svg)](https://katalytic-files.readthedocs.io/en/latest/) [![license: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT)

**Don't use in production yet.**
I will probably introduce backwards incompatible changes

A one-stop solution for all your filesystem related needs. You get atomic operations, intuitive defaults, no boilerplate, and more flexibility than you've ever dreamed of.

- Load/save csv, json, and text files
- Copy, move, or delete files and directories
- List the files and directories recursively. You can use glob too
- The functions accept `Path` and `str` as input and return the same type
- No 3rd party dependencies
- Many more (TODO: Link to tocs)

## Example (TODO)

## Installation
By itself
```bash
pip install katalytic-files
```

As part of the [katalytic](https://gitlab.com/katalytic/katalytic) collection
```bash
pip install katalytic
```

## Contributing
We appreciate any form of contribution, including but not limited to:
- **Code contributions**: Enhance our repository with your code and tests.
- **Feature suggestions**: Your ideas can shape the future development of our package.
- **Architectural improvements**: Help us optimize our system's design and API.
- **Bug fixes**: Improve user experience by reporting or resolving issues.
- **Documentation**: Help us maintain clear and up-to-date instructions for users.

